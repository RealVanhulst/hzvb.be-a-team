account_balances = {
    "BE0001": 1.00,
    "BE0002": 22.00,
    "BE0003": 333.00,
    "BE0004": 4444.00
}


def existing_account(account: str) -> bool:
    """Checks whether the account already exists.

    :param account: account number
    :return: True if account is known, else False.
    """
    return account in account_balances


def has_valid_account_structure(account: str) -> bool:
    """Checks whether the account has a valid structure: "BE" followed by 4 digits.

    :param account: account number
    :return: True if it satisfies the rules else false.
    """
    if account.find("BE"):
        account_ints = account[2:]
        if len(account_ints) == 4 and account_ints.isdecimal():
            return True
        else:
            return False
    else:
        return False


def get_balance(account: str) -> float:
    """Returns the balance of an account

    :exception KeyError if the account doesn't exist

    :param account: account number
    :return: balance as float
    """
    return account_balances[account]


def deposit(account: str, amount: float):
    """Increases the balance of the account with amount.

    :param account: account number
    :param amount: amount to be added to the balance
    """

    account_balances[account] += amount


def withdraw(account: str, amount: float) -> bool:
    """Decreases the balance of the account with amount.

    :param account: account number
    :param amount: amount to be subtracted from the balance
    :return: True if amount successfully subtracted from the balance else False
    """
    if account_balances[account] - amount >= 0:
        account_balances[account] -= amount
        return True
    else:
        return False
