from balance import *

test_balances = account_balances


if __name__ == '__main__':

    assert get_balance("BE0001") == 1.00
    test_balance = get_balance("BE0001")
    assert deposit("BE0001", 100) == True
    assert get_balance("BE0001") == test_balance + 100
    assert withdraw("BE0001", 50) == True
    assert get_balance("BE0001") == test_balance + 50
    try:
        assert get_balance("BX0001") == False
        assert deposit("BX0001", 100) == False
        assert withdraw("BX0001", 50) == False
    except KeyError:
        pass
